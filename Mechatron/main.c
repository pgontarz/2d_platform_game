#include "Data.h"

int main(int argc, char * argv[])
{
    GameState gameState;
    
    SDL_Init(SDL_INIT_EVERYTHING);             //Inicjalizacja SDLa
    
    Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096);
    
    loadPictures(&gameState);
    loadGame(&gameState);
    loadMap(&gameState);
    loadMenu(&gameState);
    
    int done = 0;
    int liczbamenu = menu(gameState.window, &gameState);
    
    while (liczbamenu != 4)
    {
        done = 0;
        
        switch(liczbamenu)
        {
            case 1:
                
                Mix_PlayChannel(0, gameState.sounds.music_sound, -1);
                
                // petla zdarzen
                while(!done)
                {
                    if (processEvents(gameState.window, &gameState) == 1)
                    {
                        Mix_FadeOutChannel(0, 500);
                        done = 1;
                    }
                    
                    collisionDetect(&gameState);
                    collisionBullets(&gameState);
                    collisionEnemies(&gameState);
                    
                    if (process(&gameState) == 1)
                    {
                        Mix_FadeOutChannel(0, 500);
                        done = 1;
                    }
                    
                    // Wyswietlanie rendera:
                    doRender(&gameState);
                    
                    if(!(gameState.man.hp > 0))
                    {
                        death(&gameState);
                        loadGame(&gameState);
                        loadMap(&gameState);
                        Mix_FadeOutChannel(0, 500);
                        done = 1;
                    }
                    
                    SDL_Delay(10);
                }
                liczbamenu = menu(gameState.window, &gameState);
                break;
                
            case 2:
                save(&gameState);
                liczbamenu = menu(gameState.window, &gameState);
                break;
                
            case 3:
                if(load(&gameState)) liczbamenu = 1;
                else liczbamenu = menu(gameState.window, &gameState);
                break;
                
            case 4:
                quitMenu(&gameState);
                quitGame(&gameState);
                
                //zamkniecie SDLa
                SDL_Quit();
                
                return 0;
                break;
        }
    }
}
