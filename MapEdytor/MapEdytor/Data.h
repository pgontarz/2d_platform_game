#ifndef Data_h
#define Data_h


#endif /* Data_h */


#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int x,y,w,h;
    char *name;
} Brick;

typedef struct
{
    //Przewijanie ekranu
    float scrollX;

    //cegielki
    Brick bricks[500];

    //obrazki
    SDL_Texture *ziemia;
    SDL_Texture *wall;
    SDL_Texture *fire;

    int wybor;
    int rodzaj;

    SDL_Window *window;                 //Deklaracja okna
    SDL_Renderer *renderer;             //Deklaracja renderera

} GameState;

void loadPictures(GameState *game)
{
    //Tworzenie okna gry przy uzyciu nastepujacych ustawien:
    game->window = SDL_CreateWindow("Crysis5",                    // tytul okna
                                    SDL_WINDOWPOS_UNDEFINED,      // x wspolrzedna okna
                                    SDL_WINDOWPOS_UNDEFINED,      // y wspolrzedna okna
                                    1200,                         // szerokosc, w pikselach
                                    700,                          // wysokosc, w pikselach
                                    0);                           //flagi

    game->renderer = SDL_CreateRenderer(game->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    SDL_Surface *surface = NULL;

    // wczytywanie obrazków i tworzenie z nich tekstur
    surface = IMG_Load("Pictures/ziemia.bmp");
    if(surface == NULL)
    {
        printf("Cannot find ziemia.bmp!\n\n");
        SDL_Quit();
        exit(1);
    }else
    {
        //jesli wsrzystko poszlo dobrze
        if( surface != NULL )
        {
            //wybor koloru, ktory ma byc przezroczysty
            Uint32 colorkey = SDL_MapRGB( surface->format, 255, 255, 255 );

            SDL_SetColorKey( surface, 1, colorkey );
        }
    }

    game->ziemia = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load("Pictures/wall.bmp");
    if(surface == NULL)
    {
        printf("Cannot find wall.bmp!\n\n");
        SDL_Quit();
        exit(1);
    }else
    {
        //jesli wsrzystko poszlo dobrze
        if( surface != NULL )
        {
            //wybor koloru, ktory ma byc przezroczysty
            Uint32 colorkey = SDL_MapRGB( surface->format, 255, 255, 255 );

            SDL_SetColorKey( surface, 1, colorkey );
        }
    }

    game->wall = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);

    surface = IMG_Load("Pictures/fire_0.bmp");
    if(surface == NULL)
    {
        printf("Cannot find fire_0.bmp!\n\n");
        SDL_Quit();
        exit(1);
    }else
    {
        //jesli wsrzystko poszlo dobrze
        if( surface != NULL )
        {
            //wybor koloru, ktory ma byc przezroczysty
            Uint32 colorkey = SDL_MapRGB( surface->format, 255, 255, 255 );

            SDL_SetColorKey( surface, 1, colorkey );
        }
    }

    game->fire = SDL_CreateTextureFromSurface(game->renderer, surface);
    SDL_FreeSurface(surface);
}

void loadGame(GameState *game)
{
    game->scrollX = 0;
    game->wybor = 0;
    game->rodzaj = 0;

    int i=0;
    for (i=0; i<500; i++)
    {
        game->bricks[i].x = 0;
        game->bricks[i].y = 0;
        game->bricks[i].w = 30;
        game->bricks[i].h = 30;
    }

}

int processEvents(SDL_Window *window, GameState *game)
{
    SDL_Event event;
    int done=0;

    // sprawdzanie zdarzen
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_WINDOWEVENT_CLOSE:
            {
                if(window)
                {
                    SDL_DestroyWindow(window);
                    window = NULL;
                    done=1;
                }
            }
                break;
            case SDL_KEYDOWN:
            {
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        done=1;
                        break;

                    case SDLK_RETURN:
                        done=2;
                        break;

                    case SDLK_UP:
                        game->bricks[game->wybor].y -=5;
                        break;

                    case SDLK_DOWN:
                        game->bricks[game->wybor].y +=5;
                        break;

                    case SDLK_LEFT:
                        game->bricks[game->wybor].x -=5;
                        break;

                    case SDLK_RIGHT:
                        game->bricks[game->wybor].x +=5;
                        break;

                    case SDLK_a:
                        printf("%d\n", game->wybor);
                        if(game->wybor > 0)
                        {
                            game->wybor --;
                        }else game->wybor = 499;
                        break;

                    case SDLK_d:
                        printf("%d\n", game->wybor);
                        if(game->wybor < 499)
                        {
                            game->wybor ++;
                        }else game->wybor = 0;
                        break;

                    case SDLK_w:
                        if(game->rodzaj > 0)
                        {
                            game->rodzaj --;
                        }else game->rodzaj = 2;
                        break;

                    case SDLK_s:
                        if(game->rodzaj > 0)
                        {
                            game->rodzaj --;
                        }else game->rodzaj = 2;
                        break;

                    case SDLK_e:
                        game->bricks[game->wybor].w +=5;
                        game->bricks[game->wybor].h +=5;
                        printf("%d\n", game->bricks[game->wybor].w);
                        break;

                    case SDLK_q:
                        game->bricks[game->wybor].w -=5;
                        game->bricks[game->wybor].h -=5;
                        printf("%d\n", game->bricks[game->wybor].w);
                        break;
                }
            }
                break;
            case SDL_QUIT:
                done=1;
                break;
        }
    }

    switch (game->rodzaj)
    {
        case 0:
            game->bricks[game->wybor].name = "wall";
            break;
        case 1:
            game->bricks[game->wybor].name = "ground";
            break;
        case 2:
            game->bricks[game->wybor].name = "fire";
            break;
    }


    //Przewijanie
    game->scrollX = -game->bricks[game->wybor].x+600;
    if(game->scrollX > 0)
        game->scrollX = 0;

    return done;
}

void doRender(GameState *game)
{
    // ustawienie koloru rendera na niebieski
    SDL_SetRenderDrawColor(game->renderer, 128, 191, 255, 255);

    // czyszczenie rendera na niebieszko
    SDL_RenderClear(game->renderer);

    int i=0;
    //rysowanie sciany
    for (i=0; i<500; i++)   // ziemia
    {
        if(game->bricks[i].name == "ground")
        {
            SDL_Rect brickRect = {game->scrollX + game->bricks[i].x, game->bricks[i].y, game->bricks[i].w, game->bricks[i].h};    ///z przesunieciem
            SDL_RenderCopy(game->renderer, game->ziemia, NULL, &brickRect);
        }else
        if(game->bricks[i].name == "wall")
        {
            SDL_Rect brickRect = {game->scrollX + game->bricks[i].x, game->bricks[i].y, game->bricks[i].w, game->bricks[i].h};    ///z przesunieciem
            SDL_RenderCopy(game->renderer, game->wall, NULL, &brickRect);
        }else
        if(game->bricks[i].name == "fire")
        {
            SDL_Rect rect = {game->scrollX + game->bricks[i].x, game->bricks[i].y, game->bricks[i].w, game->bricks[i].h};        ///z przesunieciem
            SDL_RenderCopyEx(game->renderer, game->fire, NULL, &rect, 0, NULL, 0);
        }
    }

    // wybor koloru do rysowania paska zycia
    SDL_SetRenderDrawColor(game->renderer, 255, 0, 0, 255);

    // wybrany klocek
    SDL_Rect rect = {game->scrollX + game->bricks[game->wybor].x, game->bricks[game->wybor].y, 10, 10};
    //game->bricks[game->wybor].x, game->bricks[game->wybor].y
    SDL_RenderFillRect(game->renderer, &rect);

    // wyswietlanie stworzonego rysunku
    SDL_RenderPresent(game->renderer);
}

void quitGame(GameState *game)
{
    //wylaczanie gry i oproznianie pamieci
    SDL_DestroyTexture(game->ziemia);
    SDL_DestroyTexture(game->wall);
    SDL_DestroyTexture(game->fire);

    // zamknij i usun okno
    SDL_DestroyWindow(game->window);
    SDL_DestroyRenderer(game->renderer);
}

void save(GameState *game)
{
    int i=0;

    FILE *mapa = fopen("/Users/MacBookPRO/Documents/Crysis 5/map.txt", "w");

    for (i=0; i<500; i++)
    {
        if(game->bricks[i].name == "ground" || game->bricks[i].name == "wall" || game->bricks[i].name == "fire")
        {
            fprintf(mapa, "game->bricks[%d].x = %d;\n", i, game->bricks[i].x);
            fprintf(mapa, "game->bricks[%d].y = %d;\n", i, game->bricks[i].y);
            fprintf(mapa, "game->bricks[%d].w = %d;\n", i, game->bricks[i].w);
            fprintf(mapa, "game->bricks[%d].h = %d;\n", i, game->bricks[i].h);
            fprintf(mapa, "game->bricks[%d].name = \"%s\";\n", i, game->bricks[i].name);
        }
    }

    fclose(mapa);

    printf("Zapisano");
}
