#include "Data.h"

int main(int argc, char * argv[])
{
    GameState gameState;

    SDL_Init(SDL_INIT_EVERYTHING);             //Inicjalizacja SDLa

    loadPictures(&gameState);
    loadGame(&gameState);

    int done = 0;

    // petla zdarzen
    while(done != 1)
    {
        switch (processEvents(gameState.window, &gameState))
        {
            case 1:
                done = 1;
                break;

            case 2:
                save(&gameState);
                break;
        }

        // Wyswietlanie rendera:
        doRender(&gameState);

        SDL_Delay(10);
    }

    quitGame(&gameState);

    //zamkniecie SDLa
    SDL_Quit();

    return 0;
}
